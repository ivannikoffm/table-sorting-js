let tableHeaders = document.querySelectorAll('th');
let isReversed = new Array(tableHeaders.length); // If table was sorted - just reverse it if one more request happened
isReversed.fill(false, 0, 6);

// Add click-event for all table headers. On click will sort node array and re-render the table
for (let i = 0; i < tableHeaders.length; i++) {
    let sortedRows = [];
    tableHeaders[i].addEventListener('click', () => {
        sortedRows = resortTable(i, sortedRows);
        
        clearTable();
        for (let row of sortedRows) // Fill table with new sorted rows
            document.querySelector('table').appendChild(row);
    });
}

// Check the index of column and define the type of column data
let resortTable = function(index, sortedRows) {
    if (index == 1 | index == 2 | index == 3)
        sortedRows = sortBy(index, 'string');
    else if (index == 0 | index == 4)
        sortedRows = sortBy(index, 'num');
    else if (index == 5)
        sortedRows = sortBy(index, 'date');
    return sortedRows;
}


const clearTable = () => {
    let rows = document.querySelectorAll('tr');
    for (let i = 1; i < rows.length; i++)
        rows[i].remove(); 
};

// 3 comparators for each type of column data values - numeric comparator, string and date
function sortBy(index, type) {

    // Parse all the table and make the data-object-array
    let unsortedRows = document.querySelectorAll('tr');
    let sorted = [];
    sorted = Array.from(unsortedRows);
    sorted.splice(0, 1); // Remove first row - headers

    // check the flag of column-index. If is sorted alredy - just reverse it
    if (isReversed[index] == true) {
        isReversed[index] = !isReversed[index];
        return sorted.reverse();
    }

    if (type == 'num') // Number comparator is easy
        sorted.sort(function (a, b) { return +a.cells[index].innerHTML - +b.cells[index].innerHTML; })
    else if (type == 'string') { // String-type
        sorted.sort(function (a, b) {
            let letterA = a.cells[index].innerHTML.toLowerCase();
            let letterB = b.cells[index].innerHTML.toLowerCase();

            if (letterA.toLowerCase() < letterB.toLowerCase())
                return -1;
            if (letterA.toLowerCase() > letterB.toLowerCase())
                return 1;
            return 0;
        });
    } else if (type == 'date') { // Date-type
        sorted.sort(function (a, b) {
            let aDate = new Date(a.cells[index].innerHTML);
            let bDate = new Date(b.cells[index].innerHTML);

            return aDate - bDate;
        })
    }
    isReversed[index] = !isReversed[index];
    return sorted;
}