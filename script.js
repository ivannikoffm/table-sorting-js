// //let testArr = [200100, 11, 90, 89, 55, 60, 3001, 14, 55, 21054, 218, 914, 78, 115];
// let unsortedWords = ['Albert', 'Jamelia', 'Roisin', 'Georgina', 'Findlay', 'Ellise', 'Kaleem', 'Willie', 'Zander', 'Mixxon'];
// const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
// let tableHeaders = document.querySelectorAll('th');
// let isReversed = new Array(tableHeaders.length);
// isReversed.fill(false, 0, 6);

// document.addEventListener('click', () => {
//     alert(sortByWords(unsortedWords));
// });

// // Add click-event for all table headers. On click i sorte node array and re-render the table
// for (let i = 0; i < tableHeaders.length; i++) {
//     let sortedRows = [];
//     tableHeaders[i].addEventListener('click', () => {
//         switch (i) {
//             // Switch the index of clicked header and call appropriate function
//             case (0): sortedRows = resortTable(i, isReversed[i]); break; 
//             case (4): sortedRows = resortTable(i, isReversed[i]); break;
//         }

//         clearTable();
//         for (let row of sortedRows) // Fill table with new sorted rows
//             document.querySelector('table').appendChild(row);
//     });
// }

// // Function will sort table rows array from min to max values of spicified column
// // At the input, it gets 2 args:
// // - Unsorted array of rows from the table
// // - Index of the column that header was clicked on
// function sortRowsByNumbersMinToMax(unsortedRowArray, indexOfNumber) {
//     unsortedRowArray.splice(0, 1); // Remove the first row from hendling - there are headers
//     let sortedRowsArray = [unsortedRowArray[0]]; // Initiate a new row-array with first row of Unsorted array
//     unsortedRowArray.splice(0, 1); // Remove that row from unsorted array


//     // In each iteration of this cycle, i take a value from the first row of the unsorted array.
//     // Then I compare it with the maximum and minimum numbers that were already written to the sorted array.
//     // And then I insert the row in the right place of sorted array
//     // At the end of the loop, I remove the string from the unsorted array. And so on until the end, until the unsorted array ends
//     while (unsortedRowArray.length > 0) {
//         let row = unsortedRowArray[0];

//         // In case the value from unsorted array is greater than the last one of sorted array - i put the value to the end of sorted array
//         if (+row.children[indexOfNumber].innerText >= +sortedRowsArray[sortedRowsArray.length - 1].children[indexOfNumber].innerText) {
//             sortedRowsArray.push(row);
//             unsortedRowArray.splice(0, 1); continue;
//         }
        
//         // In case the value from unsorted array is less than the first one of sorted array - i put the value as new first element of sorted array (arr.unshift)
//         if (+row.children[indexOfNumber].innerText <= +sortedRowsArray[0].children[indexOfNumber].innerText) {
//             sortedRowsArray.unshift(row);
//             unsortedRowArray.splice(0, 1); continue;
//         }

//         // If the value is not the maximum or minimum - then I iterate through the entire sorted array and compare each value with the new one 
//         // Until a suitable place is found.
//         for (let i = 0; i < sortedRowsArray.length; i++) {
//             if (+row.children[indexOfNumber].innerText >= +sortedRowsArray[i].children[indexOfNumber].innerText && +row.children[indexOfNumber].innerText <= +sortedRowsArray[i + 1].children[indexOfNumber].innerText) {
//                 sortedRowsArray.splice(i + 1, 0, row); break;
//             }
//         }
//         unsortedRowArray.splice(0, 1); // Remove handled row from unsorted arr
//     }
//     return sortedRowsArray;
// }

// function resortTable(index, isArrReversed) {
//     let rowsCollection = document.querySelectorAll('tr');
//     let sortedRows = [];
//     sortedRows = sortRowsByNumbersMinToMax(Array.from(rowsCollection), index);
//     // Check array is reversed or not form the global revers-state array. And toggle revers
//     if (isArrReversed) {
//         sortedRows.reverse();
//         isReversed[index] = !isReversed[index];
//     } else isReversed[index] = !isReversed[index];
//     return sortedRows;
// }


// const clearTable = () => {
//     let rows = document.querySelectorAll('tr');
//     for (let i = 1; i < rows.length; i++)
//         rows[i].remove(); 
// };



// function sortByWords(unsortedWords) {
//     let sortedWords = [unsortedWords[0]];
//     unsortedWords.splice(0, 1);

//     while (unsortedWords.length > 0) {
//         let word = unsortedWords[0];
//         let letter = word[0].toLowerCase();

//         if (alphabet.indexOf(letter) >= alphabet.indexOf(sortedWords[sortedWords.length - 1][0].toLowerCase())){
//             sortedWords.push(word);
//             unsortedWords.splice(0, 1); continue;
//         }

//         if (alphabet.indexOf(letter) <= alphabet.indexOf(sortedWords[0][0].toLowerCase())) {
//             sortedWords.unshift(word);
//             unsortedWords.splice(0, 1); continue;
//         }

//         for (let i = 0; i < sortedWords.length; i++) {
//             if (alphabet.indexOf(letter) >= alphabet.indexOf(sortedWords[i][0].toLowerCase()) &&
//                 alphabet.indexOf(letter) <= alphabet.indexOf(sortedWords[i+1][0].toLowerCase())) {
//                 sortedWords.splice(i + 1, 0, unsortedWords[0]); break;
//             }
//         }
//         unsortedWords.splice(0, 1);
//     }
//     return sortedWords.join();
// }